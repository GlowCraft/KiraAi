package moe.kira.ai;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.esotericsoftware.reflectasm.FieldAccess;
import com.esotericsoftware.reflectasm.MethodAccess;

@SuppressWarnings("all")
public class KiraAi extends JavaPlugin implements Listener {
    static MethodAccess getNMSEntityMethod;
    static int getNMSEntityMethodIndex;
    static FieldAccess fromMobSpawnerField;
    static int fromMobSpawnerIndex;
    
    @Override
    public void onEnable() {
        try {
            Class<?> craftEntityClazz = Class.forName("org.bukkit.craftbukkit." + VersionLevel.strip() + ".entity.CraftEntity");
            getNMSEntityMethod = MethodAccess.get(craftEntityClazz);
            getNMSEntityMethodIndex = getNMSEntityMethod.getIndex("getHandle");
            Class nmsEntityClazz = Class.forName("net.minecraft.server." + VersionLevel.strip() + ".Entity");
            fromMobSpawnerField = FieldAccess.get(nmsEntityClazz);
            fromMobSpawnerIndex = fromMobSpawnerField.getIndex("fromMobSpawner");
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
        Bukkit.getPluginManager().registerEvents(this, this);
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSpawn(SpawnerSpawnEvent evt) {
        Entity entity = evt.getEntity();
        if (!entity.isValid() || !(entity instanceof LivingEntity)) return;
        
        CreatureSpawner spawner = evt.getSpawner();
        switch(spawner.getSpawnedType()) {
            case ZOMBIE:
            case SKELETON:
            case SPIDER:
            case CAVE_SPIDER:
                if (shouldNerf(spawner.getBlock())) {
                    setFromMobSpawner((LivingEntity) entity, true);
                } else {
                    setFromMobSpawner((LivingEntity) entity, false);
                }
                
            default:
                setFromMobSpawner((LivingEntity) entity, false);
        }
    }
    
    static void setFromMobSpawner(LivingEntity entity, boolean fromMobSpawner) {
        try {
            Object e = getNMSEntityMethod.invoke(entity, getNMSEntityMethodIndex);
            fromMobSpawnerField.setBoolean(e, fromMobSpawnerIndex, fromMobSpawner);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    static boolean shouldNerf(Block block) {
         Material type;
         Block down = block.getRelative(BlockFace.DOWN);
         type = down.getType();
         
         if (type != Material.MOSSY_COBBLESTONE && type != Material.COBBLESTONE) return true;
         type = down.getRelative(BlockFace.NORTH).getType();
         if (type != Material.MOSSY_COBBLESTONE && type != Material.COBBLESTONE) return true;
         type = down.getRelative(BlockFace.SOUTH).getType();
         if (type != Material.MOSSY_COBBLESTONE && type != Material.COBBLESTONE) return true;
         type = down.getRelative(BlockFace.WEST).getType();
         if (type != Material.MOSSY_COBBLESTONE && type != Material.COBBLESTONE) return true;
         type = down.getRelative(BlockFace.EAST).getType();
         if (type != Material.MOSSY_COBBLESTONE && type != Material.COBBLESTONE) return true;
         return false;
    }
    
}

